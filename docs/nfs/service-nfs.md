<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Nfs Service
* [Service Overview](https://dashboards.gitlab.net/d/nfs-main/nfs-overview)
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22nfs%22%2C%20tier%3D%22stor%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Share"

## Logging

* [system](https://log.gprd.gitlab.net/goto/3a1a0019df2f6b555866b6f11eb92172)

## Troubleshooting Pointers

* [../blackbox/blackbox-git-exporter.md](../blackbox/blackbox-git-exporter.md)
* [../frontend/block-things-in-haproxy.md](../frontend/block-things-in-haproxy.md)
* [../gitaly/gitaly-down.md](../gitaly/gitaly-down.md)
* [../gitaly/gitaly-error-rate.md](../gitaly/gitaly-error-rate.md)
* [../gitaly/gracefully-restart-gitaly-ruby.md](../gitaly/gracefully-restart-gitaly-ruby.md)
* [../gitaly/storage-rebalancing.md](../gitaly/storage-rebalancing.md)
* [recovering-from-nfs-disaster.md](recovering-from-nfs-disaster.md)
* [../praefect/praefect-bypass.md](../praefect/praefect-bypass.md)
* [../sidekiq/sidekiq-survival-guide-for-sres.md](../sidekiq/sidekiq-survival-guide-for-sres.md)
* [../uncategorized/chef-guidelines.md](../uncategorized/chef-guidelines.md)
* [../uncategorized/deleted-project-restore.md](../uncategorized/deleted-project-restore.md)
* [../uncategorized/missing_repos.md](../uncategorized/missing_repos.md)
* [../uncategorized/stale-file-handles.md](../uncategorized/stale-file-handles.md)
<!-- END_MARKER -->
