<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Psql-timings Service
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22psql-timings%22%2C%20tier%3D%22inf%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:PsqlTimings"

## Logging

* [PostgreSQL](/var/log/postgresql/postgresql-9.5-main.log)

<!-- END_MARKER -->
